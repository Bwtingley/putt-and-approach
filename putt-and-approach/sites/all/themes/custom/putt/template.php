<?php

	function putt_preprocess_page(&$variables){

		$page = $variables['page'];
		$content_class = 'col-sm-12 col-md-6 offset-md-1 col-lg-8 offset-lg-1';
		$sidebar_right = 'col-sm-12 col-md-4 col-lg-3';
		$variables['page']['tpl_control'] = [];


		if(empty($page['sidebar_right'])){
			$sidebar_right = '';
			$content_class = 'col-sm-12 col-md-12 col-lg-12';
		}

		$variables['page']['tpl_control']['content_class'] = $content_class;
		$variables['page']['tpl_control']['sidebar_right'] = $sidebar_right;

	}