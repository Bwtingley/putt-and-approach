<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>

  <div id="page-wrapper" class="container">
    <div id="page">

    <div id="header" class="row">
      <div class="section clearfix">
        <div id="site-header-image" class="col-sm-12 col-md-12 col-lg-12">
          <a href="">
            <img src="http://localhost/putt-and-approach/putt-and-approach/<?php print path_to_theme(); ?>/imgs/banner2.jpg" title="Home" alt="Ivory Trunks" class="img-fluid"/>
          </a>
        </div>
      <div class="container top">
        <div id="site-logo-image" class="col-sm-4 col-md-4 col-lg-4">
          <a href="">
            <img src="http://localhost/putt-and-approach/putt-and-approach/<?php print path_to_theme(); ?>/imgs/logo.png" class="img-fluid" />
          </a>
        </div>
      </div>
    </div>
 </div></div> <!-- /.section, /#header -->

<div class="main-navbar col-sm-12 col-md-12 col-lg-12">
    <?php if ($main_menu): ?>
      <div id="navigation" class="navbar col-sm-12 col-md-12 col-lg-12">
        <div class="section">
          <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'inline', 'clearfix')), 'heading' => t(''))); ?>
        </div>
      </div> <!-- /.section, /#navigation -->
    <?php endif; ?>
</div>

<div class="lower-content">

    <?php print $messages; ?>

    <div id="main-wrapper"><div id="main" class="clearfix">

    <div class="row">

   
      <?php if ($page['sidebar_left']): ?>
        <div class="col-sm-12 col-md-4 col-lg-3">
          <div id="sidebar-first" class="column sidebar">
            <div class="section">
              <?php print render($page['sidebar_left']); ?>
            </div>
          </div> <!-- /.section, /#sidebar-first -->
        </div>
      <?php endif; ?>
    

      <div class="<?php print $page['tpl_control']['content_class'] ?> ">
        <div id="content" class="column">
          <div class="section">
        <?php print render($title_suffix); ?>
          <?php if ($tabs): ?>
            <div class="tabs">
              <?php print render($tabs); ?>
            </div>
          <?php endif; ?>
          <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
          <ul class="action-links">
            <?php print render($action_links); ?> 
          </ul>
        <?php endif; ?>
        <div class="main-content 
            <?php if(!drupal_is_front_page()) { ?>
              main-content-shadow<?php
            }?>"
        >
          <?php
            if (drupal_is_front_page()) {
              ?><h2>Welcome to Putt & Approach Radio</h2><?php
            }
          ?>
        <?php print render($page['content']); ?>
        </div>
          </div>
        </div> <!-- /.section, /#content -->
      </div><!-- three-fourths -->

      
        <?php if ($page['sidebar_right']): ?>
          <div class="<?php print $page['tpl_control']['sidebar_right'] ?>">
            <div id="sidebar-second" class="column sidebar sidebar-right">
              <div class="section">
                <?php print render($page['sidebar_right']); ?>
              </div>
            </div> <!-- /.section, /#sidebar-second -->
          </div>        
        <?php endif; ?>


        </div>
      </div> <!-- /#main, /#main-wrapper -->
    </div><!-- main content row -->

    <div id="footer"><div class="section">
      <div><p>&copy;2017 Putt and Approach Radiox</p></div>
    </div></div> <!-- /.section, /#footer -->
</div>
    </div>
  </div> <!-- /#page, /#page-wrapper -->
